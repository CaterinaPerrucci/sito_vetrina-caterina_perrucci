<nav class="navbar nav2">
    <div class="container-fluid d-flex">
        <span class="navbar-brand mb-0 h1 mx-auto">
            <ul class="d-flex list-unstyled">
                @foreach ($categories as $category)
                    @if (session('locale') == 'it')
                        <li class="nav-item fs-6 mx-3 ">
                            <a class="nav-link nav-link2"
                                href="{{ route('showCategory', $category) }}">{{ $category->name }}</a>
                        </li>
                    @elseif (session('locale') == 'en')
                        <li class="nav-item fs-6 mx-3 ">
                            <a class="nav-link nav-link2"
                                href="{{ route('showCategory', $category) }}">{{ $category->en }}</a>
                        </li>
                    @elseif (session('locale') == 'es')
                        <li class="nav-item fs-6 mx-3 ">
                            <a class="nav-link nav-link2"
                                href="{{ route('showCategory', $category) }}">{{ $category->es }}</a>
                        </li>
                    @else
                        <li class="nav-item fs-6 mx-3 ">
                                <a class="nav-link nav-link2"
                                    href="{{ route('showCategory', $category) }}">{{ $category->name }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </span>
    </div>
</nav>
